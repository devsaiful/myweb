<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
// Route::get('/', function () {
//     return view('home');
// });
//
// Route::get('/about', function () {
//     return view('about');
// });
//
// Route::get('/service', function () {
//     return view('service');
// });
//
// Route::get('/blog', function () {
//     return view('blog');
// });
//
// Route::get('/contact', function () {
//     return view('contact');
// });

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/service', 'PagesController@service');
Route::get('/blog', 'PagesController@blog');
Route::get('/contact', 'PagesController@contact');

Route::get('/message', 'MessageController@index');
Route::get('/message/{id}', 'MessageController@show');
Route::post('/contact/save', 'MessageController@saveMessage');


Route::get('/admin/home', 'Admin\AdminConreoller@home');
//Route::get('/admin/', 'Admin\PageConreoller@home');
