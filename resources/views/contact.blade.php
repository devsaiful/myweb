@extends('template')

@section('content')
  <!--
    ==================================================
        Global Page Section Start
    ================================================== -->
    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>Contact</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">
                                    <i class="ion-ios-home"></i>
                                    Home
                                </a>
                            </li>
                            <li class="active">Contact</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#page-header-->


    <!--
    ==================================================
        Contact Section Start
    ================================================== -->
    <section id="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="block">
                        <h2 class="subtitle wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">Contact With Me</h2>
                        <p class="subtitle-des wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                            Submit your requitements or any query
                        </p>
                        <div class="contact-form">
                          @include('_message.contact')

                          {!! Form::open(['url' => '/contact/save']) !!}

                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".6s">
                                {{Form::label('name', 'Your Name', ['class' => 'subtitle-des'])}}
                                {{Form::text('name', '',['class' => 'form-control', 'placeholder' => 'Your Name', 'required' => 'Required'])}}
                            </div>

                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".6s">
                                {{Form::label('email', 'Your e-mail', ['class' => 'subtitle-des'])}}
                                {{Form::text('email', '',['class' => 'form-control', 'placeholder' => 'Your e-mail', 'required' => 'Required'])}}
                            </div>

                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".6s">
                                {{Form::label('subject', 'Subject', ['class' => 'subtitle-des'])}}
                                {{Form::text('subject', '',['class' => 'form-control', 'placeholder' => 'Subject', 'required' => 'Required'])}}
                            </div>

                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".6s">
                                {{Form::label('message', 'Subject', ['class' => 'subtitle-des'])}}
                                {{Form::textarea('message', '',['id'=> 'article-ckeditor','class' => 'form-control', 'placeholder' => 'Message', 'required' => 'Required', 'rows'=> 6])}}
                            </div>

                            <div id="submit" class="wow fadeInDown" data-wow-duration="500ms" data-wow-delay="1.4s">
                                {{Form::submit('Send Message',['class' => 'btn btn-default btn-send', 'id' => 'contact-submit'])}}
                            </div>
                            <p class="alert alert-warning">* All fiels are required</p>

                          {!! Form::close() !!}


                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                     <div class="map-area">
                        <h2 class="subtitle  wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">Find Us</h2>
                        <p class="subtitle-des wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                            Find our location !!
                        </p>
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.9454482144793!2d101.65968121427306!3d3.1091353542655615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4a2b6c7ce02b%3A0x9bbd995c7f13b124!2sThe+Park+Residences!5e0!3m2!1sen!2smy!4v1501902885691" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row address-details">
                <div class="col-md-3">
                    <div class="address wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                        <i class="ion-ios-location-outline"></i>
                        <h5>125 , Kings Street,Melbourne <br>United Kingdom,600562</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="address wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".5s">
                        <i class="ion-ios-location-outline"></i>
                        <h5>125 , Kings Street,Melbourne <br>United Kingdom,600562</h5>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="email wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".7s">
                        <i class="ion-ios-email-outline"></i>
                        <p>support@themefisher.com<br>support@themefisher.com</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="phone wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".9s">
                        <i class="ion-ios-telephone-outline"></i>
                        <p>+07 052 245 022<br>+07 999 999 999</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
      <script>
          CKEDITOR.replace( 'article-ckeditor' );
      </script>

@endsection
