@extends('template')

@section('content')
  <!--
  ==================================================
  Global Page Section Start
  ================================================== -->
  <section class="global-page-header">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="block">
                      <h2>Message</h2>
                      <ol class="breadcrumb">
                          <li>
                              <a href="index.html">
                                  <i class="ion-ios-home"></i>
                                  Home
                              </a>
                          </li>
                          <li class="active">Message</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </section><!--/#Page header-->
	<section id="blog-full-width">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
          @if (count($messages) > 0)
            @foreach ($messages as $message)
              <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                <div class="blog-post-image">
                  <a href="post-fullwidth.html"><img class="img-responsive" src="images/blog/post-1.jpg" alt="" /></a>
                </div>
                <div class="blog-content">
                  <h2 class="blogpost-title">
                    <a href="post-fullwidth.html">{{$message->subject}}</a>
                  </h2>
                  <div class="blog-meta">
                    <span>{{$message->updated_at}}</span>
                    <span>by <a href="">{{$message->name}}</a></span>
                  </div>
                  <p>{{$message->message}}</p>
                  <a href="/message/{{$message->id}}" class="btn btn-dafault btn-details">Continue Reading</a>
                </div>
              </article>
            @endforeach
            {{$messages->links()}}
          @else
            <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
              <p>
                Have no post
              </p>
            </article>
          @endif
        </div>
				<div class="col-md-4">
					<div class="sidebar">
						<div class="search widget">
							<form action="" method="get" class="searchform" role="search">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search for...">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button"> <i class="ion-search"></i> </button>
									</span>
								</div><!-- /input-group -->
							</form>
						</div>
            <div class="author widget">
							<img class="img-responsive" src="{{asset('/images/author/author-bg.jpg')}}">
							<div class="author-body text-center">
								<div class="author-img">
									<img src="{{asset('/images/author/author.jpg')}}">
								</div>
								<div class="author-bio">
									<h3>Jonathon Andrew</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt magnam asperiores consectetur, corporis ullam impedit.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection
