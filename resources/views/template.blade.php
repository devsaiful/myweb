<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>{{config('app.name','myBlog')}}</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Template CSS Files

        ================================================== -->
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <!-- Twitter Bootstrs CSS -->
        <!--link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}"-->
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="{{ asset('/css/ionicons.min.css') }}">
        <!-- animate css -->
        <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="{{ asset('/css/slider.css') }}">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="{{ asset('/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/owl.theme.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/jquery.fancybox.css') }}">
        <!-- template main css file -->
        <link rel="stylesheet" href="{{ asset('/css/main.css') }}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{ asset('/css/responsive.css') }}">

        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="{{ asset('/js/vendor/modernizr-2.6.2.min.js') }}"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
        <!-- bootstrap js -->

        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <!-- wow js -->
        <script src="{{ asset('/js/wow.min.js') }}"></script>
        <!-- slider js -->
        <script src="{{ asset('/js/slider.js') }}"></script>
        <script src="{{ asset('/js/jquery.fancybox.js') }}"></script>
        <!-- template main js -->
        <script src="{{ asset('/js/main.js') }}"></script>
    </head>
    <body>
        @include('_layout.header')

        @yield('content')

        @include('_layout.footer')

      </body>
</html>
