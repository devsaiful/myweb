<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //Home Page
    public function home(){
      return view('home');
    }

    //About Page
    public function about(){
      return view('about');
    }

    //Service Page
    public function service(){
      return view('service');
    }

    //Blog Page
    public function blog(){
      return view('blog');
    }

    //Contact Page
    public function contact(){
      return view('contact');
    }

}
