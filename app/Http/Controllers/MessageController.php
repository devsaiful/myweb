<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Message;

use DB;


class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$messages = Message::all();
      //$messages = Message::whereDate('updated_at', \Carbon\Carbon::today())->get();
      //$messages = DB::select('select * from messages where updated_at like "%'.date('Y-m-d').'%"');
      //$messages = Message::orderBy('id', 'DESC')->take(10)->get();
      //$messages = Message::whereDate('created_at', DB::raw('CURDATE()'))->orderBy('id', 'DESC')->get();
      $messages = Message::orderBy('id', 'DESC')->paginate(10);
      return view('messages')->with('messages', $messages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $messages = Message::find($id);

      return view('show')->with('messages', $messages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    * Save message
    */
    public function saveMessage(Request $request)
    {
      // Form validation
      $this->validate($request,[
        'name'=>'required',
        'email'=>'required|email',
        'subject'=>'required',
        'message'=>'required'
      ]);

      // Create message object
      $message = new Message;
      $message->name = $request->input('name');
      $message->email = $request->input('email');
      $message->subject = $request->input('subject');
      $message->message = $request->input('message');

      // Save message
      $message->save();

      // Redirect
      return redirect('/contact')->with('success', 'Sesasge Sent');
    }
}
